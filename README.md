# Montage GTD 一个基于Laravel 集RSS阅读、思维导图、番茄工作法于一体的GTD Web site

## 快速体验

访问   [https://task.congcong.us](https://task.congcong.us)

## 技术相关

支持PHP7 Mysql5.5运行环境

基于Laravel 5.2框架

## 实现功能

### 番茄工作法 待办事项
通过番茄工作法合理的安排工作与休息，极大提高你的工作效率，另外这里有完善的待办管理，你可以定义优先级，还可以设置deadline、设置提醒时间，让你每个任务都不落下，每个任务都顺利完成！
![avatar](public/img/pomo.png)

### 记想法
支持通过插件快速分享chrome等浏览器所浏览的网站、图片与文字，同时可以你可以实时去记录你的想法，其更支持语音录入极大方便你的学习生活！
![avatar](public/img/note.png)

### 思维导图
通过思维导图来总结你的每一个想法，发散思维，认真思考每一个想法！
![avatar](public/img/mind.png)

### RSS阅读 定时推送到Kindle
支持你订阅各大个人博客、科技媒体，甚至都可以定时发送您订阅的文章到kindle阅读器，每天回家打开kindle即可享受阅读好时光！
![avatar](public/img/read.png)


