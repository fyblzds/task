@extends('layouts.app')

@section('content')
    <div class="container">
            <!-- Current Goals -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        	修改目标
                    </div>

                    <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <!-- New Task Form -->
                    <form action="{{ url('goal/'.$goal->id) }}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}

                        <!-- Task Name -->
                        <div class="form-group">
                            <label for="goal-name" class="col-sm-3 control-label">目标名称:</label>

                            <div class="col-sm-8">
	                                <input type="text" name="name" id="goal-name" class="form-control" value="{{ $goal->name }}">
                            </div>
                        </div>

                        <!-- Add Task Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-plus"></i>提交！
                                </button>
                            </div>
                        </div>
                    </form>
                    
                    
                </div>
                </div>
        </div>
    </div>
@endsection
