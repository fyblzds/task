@extends('layouts.app')

@section('content')
    <div class="container">
    
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    	修改分类
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <!-- New Task Form -->
                    <form action="{{ url('category/'.$category->id) }}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}

                        <!-- Task Name -->
                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">分类名称</label>
								
                            <div class="col-sm-8">
	                               <input type="text" name="name" id="name" class="form-control" value="{{ $category->name }}">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">分类排序</label>
								
                            <div class="col-sm-8">
	                               <input type="text" name="category_order" id="category_order" class="form-control" value="{{ $category->category_order }}">
                            </div>
                        </div>

                        <!-- Add Task Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-plus"></i>提交！
                                </button>
                            </div>
                        </div>
                    </form>
                    
                    
                </div>
            </div>

        </div>
    </div>
@endsection
